﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CursoMVCApi.Models.WS;
using CursoMVCApi.Models;

namespace CursoMVCApi.Controllers
{
    public class AccessController : ApiController
    {
        readonly db myDB = new db();

        [HttpGet]
        public Reply HelloWorld()
        {
            Reply oR = new Reply {
                Result = 1,
                Message = "Mi world"
            };

            return oR;
        }

        [HttpPost]
        //Send data to model Users, but send by Body
        public Reply Login([FromBody] Users User)
        {
            Reply oR = new Reply();
            try
            {
                //Users model = myDB.user.Where(x => x.email == email && x.myPassword == myPassword && x.IDEstatus == 1).SingleOrDefault();
                var model = myDB.user.Where(x => x.email == User.email && x.myPassword == User.myPassword && x.IDEstatus == 1);

                if(model.Count() > 0)
                {
                    oR.Result = 1;
                    oR.Message = "Token Creado";

                    //Creat Token
                    oR.Data = Guid.NewGuid().ToString();

                    Users oUser = model.First();
                    oUser.Token = oR.Data.ToString();
                    myDB.Entry(oUser).State = System.Data.Entity.EntityState.Modified;
                    myDB.SaveChanges();
                } else
                {
                    oR.Message = "Datos Incorrectos";
                }
            }
            catch (Exception ex)
            {
                oR.Result = 0;
                oR.Message = "Ocurrio un error = -1, estamos corrigiendo";
            }

            return oR;
        }
    }
}
