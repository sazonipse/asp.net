﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using CursoMVCApi.Models.WS;
using CursoMVCApi.Models;
using System.Threading.Tasks;
using System.IO;
using System.Text;

namespace CursoMVCApi.Controllers
{
    public class AnimalsController : BaseController
    {
        [HttpPost]
        public Reply Get([FromBody] SecurityToken model)
        {
            Reply oR = new Reply {
                Result = 0
            };

            if (!Verify(model.token)){
                oR.Message = "No Autorizado";
                return oR;
            }

            try {
                using (db mydb = new db()){
                    List<AnimalsLST> lst = List(mydb);
                    oR.Data = lst;
                    oR.Result = 1;
                }
            } catch (Exception ex) {
                oR.Message = "Ocurrio un ERROR en el servidor, intenta mas tarde";
            }

            return oR;
        }

        [HttpPost]
        public Reply Add([FromBody] AnimalsADD model)
        {
            Reply oR = new Reply {
                Result = 0
            };

            //Verificar el Token
            if (!Verify(model.token)) {
                oR.Message = "No Autorizado";
                return oR;
            }

            //Validaciones
            if (!Validate(model)) {
                oR.Message = Error;
                return oR;
            }

            try {
                using (db mydb = new db()) {
                    Animals oAnimal = new Animals {
                        IDEstatus = 1,
                        name = model.name,
                        paws = model.paws
                    };

                    mydb.animal.Add(oAnimal);
                    mydb.SaveChanges();

                    List<AnimalsLST> lst = List(mydb);
                    oR.Data = lst;
                    oR.Result = 1;
                }
            } catch (Exception ex) {
                oR.Message = "Ocurrio un ERROR en el servidor, intenta mas tarde";
            }

            return oR;
        }

        [HttpPost] //Tambien puede ser HttpPut]
        public Reply Edit([FromBody] AnimalsADD model)
        {
            Reply oR = new Reply {
                Result = 0
            };

            //Verificar el Token
            if (!Verify(model.token)) {
                oR.Message = "No Autorizado";
                return oR;
            }

            //Validaciones
            if (!Validate(model)) {
                oR.Message = Error;
                return oR;
            }

            try {
                using (db mydb = new db()) {
                    Animals oAnimal = mydb.animal.Find(model.IDAnimal);
                    oAnimal.name = model.name;
                    oAnimal.paws = model.paws;

                    mydb.Entry(oAnimal).State = System.Data.Entity.EntityState.Modified;
                    mydb.SaveChanges();

                    List<AnimalsLST> lst = List(mydb);
                    oR.Data = lst;
                    oR.Result = 1;
                }
            } catch (Exception ex) {
                oR.Message = "Ocurrio un ERROR en el servidor, intenta mas tarde";
            }

            return oR;
        }

        [HttpPost] //Tambien puede ser HttpDelete]
        public Reply Delete([FromBody] AnimalsADD model)
        {
            Reply oR = new Reply {
                Result = 0
            };

            //Verificar el Token
            if (!Verify(model.token)) {
                oR.Message = "No Autorizado";
                return oR;
            }

            try {
                using (db mydb = new db()) {
                    Animals oAnimal = mydb.animal.Find(model.IDAnimal);
                    oAnimal.IDEstatus = 2;

                    mydb.Entry(oAnimal).State = System.Data.Entity.EntityState.Modified;
                    mydb.SaveChanges();

                    List<AnimalsLST> lst = List(mydb);
                    oR.Data = lst;
                    oR.Result = 1;
                }
            } catch (Exception ex)             {
                oR.Message = "Ocurrio un ERROR en el servidor, intenta mas tarde";
            }

            return oR;
        }

        [HttpPost]
        public async Task<Reply> Photo([FromUri] AnimalsPicture model)
        {
            Reply oR = new Reply {
                Result = 0
            };

            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            //Verificar el Token
            if (!Verify(model.token)) {
                oR.Message = "No Autorizado";
                return oR;
            }

            //Vienen multipart
            if (!Request.Content.IsMimeMultipartContent()) {
                oR.Message = "No viene imagen";
                return oR;
            }

            await Request.Content.ReadAsMultipartAsync(provider);

            FileInfo fileinfopicture = null;

            foreach(MultipartFileData filedata in provider.FileData) {
                if (filedata.Headers.ContentDisposition.Name.Replace("\\","").Replace("\"", "").Equals("picture"))
                    fileinfopicture = new FileInfo(filedata.LocalFileName);
            }

            if (fileinfopicture != null) {
                using (FileStream fs = fileinfopicture.Open(FileMode.Open, FileAccess.Read)) {
                    byte[] b = new byte[fileinfopicture.Length];
                    UTF8Encoding temp = new UTF8Encoding(true);

                    while (fs.Read(b, 0, b.Length)>0) ;

                    try {
                        using (db mydb = new db()) {
                            Animals oAnimal = mydb.animal.Find(model.IDAnimal);
                            oAnimal.picture = b;
                            
                            mydb.Entry(oAnimal).State = System.Data.Entity.EntityState.Modified;
                            mydb.SaveChanges();

                            List<AnimalsLST> lst = List(mydb);
                            oR.Data = lst;
                            oR.Result = 1;
                        }
                    } catch(Exception Ex) {
                        oR.Message = "Intenta mas tarde";
                    }
                }
            }
            return oR;
        }

        #region
        private List<AnimalsLST> List(db mydb)
        {
            List<AnimalsLST> lst = (from d in mydb.animal
                                    where d.IDEstatus == 1
                                    select new AnimalsLST
                                    {
                                        name = d.name,
                                        paws = d.paws
                                    }).ToList();
            return lst;
        }

        private bool Validate(AnimalsADD model) {
                if(model.name == "") {
                    Error = "El nombre no puede ir vacio";
                    return false;
                } else if (model.paws <= 0){
                    Error = "El Nro de patas no puede ser menor a uno";
                    return false;
                }

            return true;
            }
        #endregion
    }
}
