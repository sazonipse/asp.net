﻿using CursoMVCApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CursoMVCApi.Controllers
{
    public class BaseController : ApiController
    {
        public string Error = "";

        public bool Verify(string token)
        {
            using (db mydb = new db())
            {
                if(mydb.user.Where(d=> d.Token == token && d.IDEstatus == 1).Count() > 0)
                    return true;
            }

            return false;
        }
    }
}
