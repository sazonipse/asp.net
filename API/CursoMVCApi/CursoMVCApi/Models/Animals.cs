﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CursoMVCApi.Models
{
    [Table("dbo.tbAnimals")]
    public class Animals
    {
        [Key]
        public int IDANimal { get; set; }
        public int IDEstatus { get; set; }
        public string name { get; set; }
        public int paws { get; set; }
        public byte[] picture { get; set; }
    }
}