﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CursoMVCApi.Models.WS;

namespace CursoMVCApi.Models
{
    public class AnimalsADD : SecurityToken
    {
        public int IDAnimal { get; set; }
        public string name { get; set; }
        public int paws { get; set; }
    }
}