﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CursoMVCApi.Models
{
    [Table("dbo.tbUser")]
    public class Users
    {
        [Key]
        public int IDUser { get; set; }
        public string email { get; set; }
        public string myPassword { get; set; }
        public Nullable<int> IDEstatus { get; set; }
        public string Token { get; set; }
    }
}