CREATE DATABASE DBPractice
GO

USE DBPractice
GO

CREATE TABLE tbUser(
	IDUser int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	IDEstatus int NOT NULL,
	email varchar(50) NOT NULL,
	myPassword varchar(50) NOT NULL,
	Token varchar(150) NULL
)
SELECT * FROM tbUser

INSERT INTO tbUser (IDEstatus, email, myPassword) VALUES(1, 'sazonipse@gmail.com', '123')


CREATE TABLE tbAnimals(
	IDAnimal int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	IDEstatus int NOT NULL,
	name varchar(50) NOT NULL,
	paws int NOT NULL,
	picture image NULL
)
SELECT * FROM tbAnimals

INSERT INTO tbAnimals VALUES (1, 'Perro', 4),(1, 'Mono', 2), (1, 'Pato', 2)