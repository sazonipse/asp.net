// Divisible by 3 => Fizz
// Divisible by 5 => Buzz
// Divisible by 3 or 5 => FizzBuzz
// Not divisible by 3 or 5 => input
// Not a number => 'Not a Number'

function fizzBuzz (number){
    if (typeof number !== 'number'){
        console.log('Is not a number');
    } else{
        if (number %3 === 0 && number %5 === 0){
            console.log('FizzBuzz');
        } else if (number %5 === 0){
            console.log('Buzz');
        } else if (number %3 === 0){
            console.log('Fizz');
        } else {
            console.log(number);
        }
    }
}

fizzBuzz(11);