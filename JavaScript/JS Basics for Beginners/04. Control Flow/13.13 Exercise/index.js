// Speed Limit = 70
// over 5 = 1 point
//12 points = Suspended
// use Math.floor(1.3) = 1

function checkSpeed (speed){
    let speedLimit = 70;
    let kmPerPoint = 5;
    let over = 0;

    if (speed < (speedLimit+kmPerPoint)){
        console.log('OK');
    } else {
        speed -= speedLimit;
        
        do{
            over++;
            speed -= kmPerPoint;
        }while(speed >= kmPerPoint)

        if(over >= 12){
            console.log('License Suspended');
        } else {
            console.log('Point => ', over);
        }
    }
}

checkSpeed(75);