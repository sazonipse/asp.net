
function calculateGade(myGrade){
    let average = 0;
    let letter = 'A';

    for(let grade of myGrade){
        average += grade;
    }
    average = average / myGrade.length;

    if(average < 60) letter = 'F';
    else if(average < 70) letter = 'D';
    else if(average < 80) letter = 'C';
    else if(average < 90) letter = 'B';

    console.log(letter);
}

const grades=[80,80,50];
calculateGade(grades);