// let address = {
//     street: 'a',
//     city: 'b',
//     zipCode: 'c'
// };

let address = new Address('a', 'b', 'c');

//Contructor Function
function Address(street, city, zipCode){
    this.street = street;
    this.city = city;
    this.zipCode = zipCode;
}

// Factory Function
function createAddress(street, city, zipCode){
    return{
        street,
        city,
        zipCode
    };
}

function showAddress(address){
    for(let key in address)
        console.log(key, address[key]);
}

showAddress(address)