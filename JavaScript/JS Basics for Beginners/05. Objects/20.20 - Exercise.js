let priceRanges = [
    {label: '$', tooltip: 'Inexpensive', minPerperson: 0, maxPerPerson: 10},
    {label: '$$', tooltip: 'Moderate', minPerperson: 11, maxPerPerson: 20},
    {label: '$$$', tooltip: 'Expensive', minPerperson: 21, maxPerPerson: 50}
];

let restaurants = [
    {averagePerPerson: 5}
]