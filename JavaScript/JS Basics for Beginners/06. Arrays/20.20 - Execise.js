//Moving an Element

const numbers = [1,2,3,4];
const outPut = move(numbers,1,-2);
console.log(outPut);

function move(array, index, offset){
    const position = index+offset;

    if(position >= array.length || position < 0){
        console.log('Invalid offset.');
        return;
    }
    
    const outPut = [...array];
    const element = outPut.splice(index,1)[0];

    outPut.splice(position, 0, element);

    return outPut;
}