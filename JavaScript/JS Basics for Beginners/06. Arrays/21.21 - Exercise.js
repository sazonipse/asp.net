//Count Ocurrences

const numbers = [1,2,3,4,1];

const count = countOcurrences(numbers, 1);

console.log(count);

function countOcurrences(array, searchElement){
    return array.reduce((accumulator, current) => {
        const ocurrence = (current === searchElement);
        //console.log(accumulator, current, searchElement);
        return accumulator + ocurrence;
    },0);
    
    // let count = 0;
    // array.forEach(function(number){ 
    //     if (number === searchElement)
    //         ++count;
    // });

    // return count;
}