// Get Max

const numbers = [1,2,3,4];

const max = getMax(numbers);

console.log(max);

function getMax(array){
    if (array.length === 0) return undefined;
    
    return array.reduce((a, b) => (a > b) ? a : b);
    
    //let ocurrence = 0;
    // return array.reduce((accumulator, current) => {
    //     ocurrence = (current > ocurrence) ? current : ocurrence;
    //     //console.log(accumulator, current, ocurrence);
    //     return ocurrence;
    // },0);
}