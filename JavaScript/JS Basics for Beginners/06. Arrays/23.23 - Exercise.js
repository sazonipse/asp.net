// Movies
console.log('All movies');
const movies = [
    {title: 'a', year: 2018, rating: 4.5},
    {title: 'b', year: 2018, rating: 4.7},
    {title: 'c', year: 2018, rating: 3},
    {title: 'd', year: 2017, rating: 4.5}
];
console.log(movies);

const titles = movies.filter(m => m.year === 2018 && m.rating >= 4)
                    .sort((a,b) => a.rating - b.rating)
                    .reverse()
                    .map(m => [m.title, m.rating]);

console.log(titles);
// console.log('All the movies in 2018 with rating > 4');
// const filtered = movies.filter(function(value){
//     return (value.year === 2018) && (value.rating >= 4);
// });
// console.log(filtered);

// console.log('Sort by their rating');
// filtered.reverse(function(a,b){
//     //a<b => -1
//     //a>b => 1
//     //a===b => 0
//     const ratingA = a.rating.toLocaleLowerCase();
//     const ratingB = b.rating.toLocaleLowerCase();

//     if(ratingA < ratingB) return -1;
//     if(ratingA > ratingB) return 1;
//     return 0;
// });
// console.log(filtered);

// console.log('Pick their title');
// for(let movie of filtered)
//     console.log(movie.title);