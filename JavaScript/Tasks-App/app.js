class Task{
    constructor(title, description){
        this.title = title;
        this.description = description;
    }
}

class UI{
    addTask(task){
        if(localStorage.getItem('tasks') === null){
            let tasks = [];
            tasks.push(task);
            localStorage.setItem('tasks', JSON.stringify(tasks));
        } else {
            let tasks = JSON.parse(localStorage.getItem('tasks'));
            tasks.push(task);
            localStorage.setItem('tasks', JSON.stringify(tasks));
        }

        this.showTask();
    }

    showTask(){
        let tasks = JSON.parse(localStorage.getItem('tasks'));

        let tasksView = document.getElementById('task-list');
        tasksView.innerHTML = '';

        for(let element of tasks) (
            tasksView.innerHTML += `<div class="card mb-3">
                                        <div class="card-body"> 
                                        <strong>${element.title}</strong> - ${element.description}
                                            <a href="#" class="btn btn-danger" name="delete-${element.title}">Delete</a>
                                        </div>
                                    </div>`
            )
    }

    deleteTask(title){
        let qclick = title.name.substr(0,6);
        let dtask = title.name.substr(7, title.name.lenght)
        
        if (qclick === "delete"){
            let tasks = JSON.parse(localStorage.getItem('tasks'));
    
            for(let i = 0; i < tasks.length; ++i){
                if(tasks[i].title == dtask){ tasks.splice(i,1); }

                localStorage.setItem('tasks', JSON.stringify(tasks));
                this.showTask();
            }
        }
    }
    
    resetForm(e){ document.getElementById(e).reset(); }
}

//DOM Events
//$(document).ready(function(){
    // const ui = new UI();

    // ui.showTask();
//});

// s = $(document).ready(function() {
//     console.log('Ahora si');
// });

document.getElementById('task-form')
        .addEventListener('submit', function(e){
    //Cancelamos el refrescamiento
    e.preventDefault();

    const title = document.getElementById('title').value;
    const description = document.getElementById('description').value;

    const task = new Task(title, description);

    const ui = new UI();

    ui.addTask(task);
    ui.resetForm('task-form')
});

document.getElementById('task-list').addEventListener('click', function(e){
    const ui = new UI();
    ui.deleteTask(e.target);
});