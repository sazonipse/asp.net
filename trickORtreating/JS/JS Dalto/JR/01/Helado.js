/* 
    A) 3 CHICOS DE 23 AÑOS PERFECTAMENTE NORMALES ENTRAN A UN HELADERIA A COMPRAR UN HELADO
       PERO HAY UN PROBLEMA: LO SPRECIOS ESTAN AL LADO DE CADA ESTANTE CON SU RESPECTIVO HELADO.

       ELLOS QUIEREN COMPRAR EL HELADO MAS CARO QUE PUEDAN CON LA PLATA QUE TIENEN, ASI QUE...
       VEAMOS COMO PODEMOS AYUDARLOS:

       ROBERTO  TIENE $1.5 USD
       PEDRO    TIENE $1.7 USD
       COFLA    TIENE $3 USD

       LOS PRECIOS DE HELADOS SON LOS SIGUIENTES:
       PALITO DE HELADO DE AGUA:        $0.6 USD
       PALITO DE HELADO DE CREMA:       $1 USD
       BOMBOM HELADO MARCA HELADIS:     $1.6 USD
       BOMBOM HELADO MARCA HELADOVICH:  $1.74 USD
       BOMBOM DE HELADO CON CONFITES:   $2.9 USD
       PTE DE 1/4 KG:                   $2.9 USD

       CREAR SOLCIONES:
       - PEDIRLES QUE INGRESEN EL  MONTO QUE TIENEN Y MORTRARLE EL HELADO MAS CARO
       - SI HAY 2 O MAS CON EL MISMO PRECIO, MOSTRAR AMBOS.
       - COFLA QUIERE SABER CUANTO ES EL VUELTO.
*/

"use strinct"

const definirCompra = (n)=>{
    let din = prompt(`Dinero de ${n}`);
    let vuelto = 0;

    if(din >= 0.6 && din < 1){
        vuelto = din - 0.6;
        return (`<br>${n}: Helado de agua, tu vuelto es: ${vuelto} USD`);
    }    
    if(din >= 1 && din < 1.6){
        vuelto = din - 1;
        return (`<br>${n}: Helado de crema, tu vuelto es: ${vuelto} USD`);
    }
    if(din >= 1.6 && din < 1.7){
        vuelto = din - 1.6;
        return (`<br>${n}: Helado de heladix, tu vuelto es: ${vuelto} USD`);
    }
    if(din >= 1.7 && din < 1.8){
        vuelto = din - 1.7; 
        return (`<br>${n}: Helado de heladovich, tu vuelto es: ${vuelto} USD`);
    }
    if(din >= 1.8 && din < 2.9){
        vuelto = din - 1.8; return (`<br>${n}: Helado de helardo, tu vuelto es: ${vuelto} USD`);
    }
    if(din >= 2.9){
        vuelto = din - 2.9;
        return (`<br>${n}: Helado de confites o pote de 1/4kg, tu vuelto es: ${vuelto} USD`);
    }
    else return (`<br>${n}: No te alcanza para ningun helado`);
}

document.writeln(definirCompra("Cofla"));
document.writeln(definirCompra("Roberto"));
document.writeln(definirCompra("Pedro"));