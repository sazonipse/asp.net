//LESSION 01

// Tipos de declaraciones
var numero1 = 1;
let numero2 = 2;
const PI = 3.1416;

//Condicionales
    if (10 < 15){
        alert("Hola");
    }else if (nombre == "Juan"){
        alert("Bienvenido");
    }else{
        alert("Adios");
    }

//Bucles
    let numero = 0;
    
    //WHILE
    while (numero < 10){
        numero++;
        document.writeln(numero + "<br>");
    }

    //DO . . . WHILE
    do{
        numero++;
        document.writeln(numero + "<br>");
    }while (numero < 10)

    //BREAK
    while (numero < 1000){
        numero++;
        document.writeln(numero + "<br>")

        if (numero == 10) { break; }
    }
    document.writeln("FIN");

    //FOR INCREMENTO
    for(let i = 0; i < 10; i++){
        document.writeln(numero + "<br>")
    }

    //CONTINUE
    for(let i = 0; i < 20; i++){
        if (i == 3) { continue; }// Se salta este numero en el bucle

        document.writeln(numero + "<br>")
    }
    
    //FOR DECREMENTO
    for(let i = 6; i > 0; i--){
        document.writeln(numero + "<br>")
    }




    
//LESSION 02

//Arrays
    let frutas = ["bananas", "manzanas", "pera"];
    document.writeln(frutas[0]); //bananas

    //Arrays Asociativos
    let PC = {
        nombre: "miPC",
        procesador: "Intel Core I7",
        ram: "16GB",
        DD: "1TB"
    }
    document.writeln(PC["procesador"]); //Intel Core I7

    //Acedemos a los datos
    let nombre = PC["nombre"];
    let procesador = PC["procesador"];
    let ram = PC["ram"];
    let DD = PC["DD"]

    let frase = `El Nombre de mi PC es: <b>${nombre}</b> <br>
                El procesador es:      <b>${procesador}</b> <br>
                La memoria es:         <b>${memoria}</b> <br>
                El Disco Duto es:      <b>${DD}</b> <br>
                `
    document.writeln(frase);


//Bucles
    let animales = ["gato", "perro", "tiranosaurio rex"]

    //FOR IN retorna el indice (0,1,2)
        for(animal in animales){
            document.write(animal + "<br>") //0 1 2
            document.write(animales[animal] + "<br>") // gato perro tiranosaurio rex
        }

    //FOR OF retorna los valores de los indices
        for(animal of animales){
            document.write(animales + "<br>") // gato perro tiranosaurio rex
        }

        let array1 = ["maria", "josefa", "roberta"]
        let array2 = ["pedro", "marcelo", array1]

        for(let array in array2){
            if(array == 2){ //Segundo array hay que hacer ciclo para mostrar
                for(let array of array1){
                    document.writeln(array + "<br>")
                }
            } else {
                document.writeln(array2[array]);
            }
        }


//FUNCIONES
    // Normal
    function Saludar(){
        let respuesta = prompt("Hola!! ¿Como fue tu dia?");

        if(respusta == "bien"){
            alert("Me alegro");
        } else {
            alert("Una pena");
        }
    }
    Saludar();


    //Como Variable
    const Saludar = function (){
        let respuesta = prompt("Hola!! ¿Como fue tu dia?");

        if(respusta == "bien"){
            alert("Me alegro");
        } else {
            alert("Una pena");
        }
    }
    Saludar();


    //Como Flecha
    const Saludar = (nombre)=>{
        let frase = `¡Hola ${nombre}! ¿Como estas?`;
        document.writeln(frase);
    }
    Saludar("Pedro");





//LESSION 03

//PROGRAMACION ORIENTADA A OBJETOS
//Clase
class Animal {
    //Contructor
    constructor(especie, edad, color){
        this.especie = especie;     //Atributo
        this.edad = edad;           //Atributo
        this.color = color;         //Atributo

        this.info = `Soy ${this.especie}, tengo ${this.edad} años y soy de color ${this.color}`;
    }

    //Metodo
    VerInfo(){
        document.writeln(this.info);
    }
}

//Objeto        //Instanciacion
let perro =     new Animal("perro",5,"marron"); 
let gato =      new Animal("gato",2,"negro"); 
let pajaro =    new Animal("pajaro",1,"verde");

perro.VerInfo();
gato.VerInfo();
pajaro.VerInfo();
 
//Herencia
    class Perro extends Animal{
        constructor(especie, edad, color, raza){
            super(especie, edad, color); //Lo hereda de la clase Animal
            this.raza = raza;
        }

        ladrar(){ alert("¡WAW!"); }
    }

    //los objetos se definen con CONST no con let
    const perro = new Perro("perro", 5, "marron", "dooberman");

    perro.VerInfo();
    perro.ladrar();

//Metodos static --> si se define como metodo static se puede acceder sin necesidad de hacer una objeto de la clase
    class Perro extends Animal{
        constructor(especie, edad, color, raza){
            super(especie, edad, color); //Lo hereda de la clase Animal
            this.raza = raza;
        }

        static ladrar(){ alert("¡WAW!"); }
    }

    Perro.ladrar();


//GET & SET
class Perro extends Animal{
    constructor(especie, edad, color, raza){
        super(especie, edad, color); //Lo hereda de la clase Animal
        this.raza = raza;
    }

    set setRaza(newRaza){ this.raza = newRaza };
    get getRaza() { return this.raza; }
}

const perro = new Perro("perro", 5, "marron", "dooberman");

perro.setRaza = "BullDog";
document.writeln(perro.getRaza);




//LESSION 04

//METODOS DE CADENA
    let cadena = "Cadena de prueba";

    concat()        // Junta dos o mas cadenas y retorna una nueva
        resultado = cadena.concat(" hola");     //Cadena de prueba hola

        let cadena2 = " cadena 2";
        ca


    starsWith()     // Si una cadena comienza con los caracteres de otra cadena, devuelve true
    endsWith()      // Si una cadena termina  con los caracteres de otra cadena, devuelve true
    includes()      // Si una cadena puede encontrarse dentro de otra cadena, devuelve true
    indexOf()       // Devuelve el indice del primer caracter de la cadena, si no existe devuelve -1
    lastIndexOf()   // Devuelve el ultimo indice del primer caracter de la cadena, si no existe devuelve -1

//FIN 05:16 por ahi vamos