//Condiciones
    if(condicion){
        //statement
    }else if(otraCondicion){
        //statement
    } else{
        //Statement
    }

    let verdad = (1 == 2 ? "Es Igual" : "No no es Igual");

    switch(role){
        case 'guest': console.log('Guest User'); break;
        case 'moderator': console.log('Moderaton User'); break;
        default: console.log('Unknown User');
    }
    
    
    //Funciones Ternarias
    var uno = nombre => nombre;
    document.write(uno("Yony Pacheco"));

    var dos = (nombre, apellido) => nombre + apellido;
    document.write(uno("Yony", " Pacheco"));


    //EVENTOS
        HTML
            // <button onClick="apretar();">SALUDAR</button>
        JS
            function apretar(){
                alert("Hello Everybody!!!");
            }
        
        WINDOWS
            windows.addEventListener("keydown", function(){ alert("Accion"); });

            windows.addEventListener("keypress", function(e){ 
                alert(e.keycode); 
                alert(String.fromCharCode(e.keycode));
            });

            windows.addEventListener("load", function(){ alert("Ya cargo."); })

        TIME
            var tiempo = setInterval(function(){
                document.write("Texto de ejemplo <br>")
            }, 200);
            
            setTimeout(function(){
                document.write("Es otro texto");
            },2000);
        
        CONFIRM
            var a = confirm("Do you need Help?");
            if (a) { 
                document.write("You say: YES"); 

                windows.location="ayuda.html";
            } else {
                document.write("You say: NO");
            }
        
        PROMPT
            var a = prompt("Nombre Completo");
            document.write(a);

        
    //TIPOS DE VARIABLES
        //Para saber el tipo de variable
            var a = "10.4653345"; document.write(typeof a); //string
            var b = Number(a); document.write(typeof b);    //Number
            var c = parseInt(b); document.write(c);         //10
            var d = Number.isInteger(c); document.write(d); //true
            var e = b.toFixed(1); document.write(e);        //10.5
            var e = b.toFixed(2); document.write(e);        //10.47
            var f = e.toString(); document.write(typeof f)  //string
        
        //Busqueda
        var a = "Hola a todos mis amigos de Youtube";
        document.write(a.indexOf("amigos")); //envia la posicion 17
        document.write(a.search("amigos")); //envia la posicion 17
        document.write(a.substr(5,11));     //a todos mis
        
        //INCLUDES --> Busca texto en una cadena devuelve TRUE si lo encuentra
        var a = "Hola a todos mis amigos de Youtube, todos estamos alegres... todos";
        document.write(a.includes("amigos",7)); //El valor de 7 equivale a que busque a partir de la posicion 7 en la cadena
        
        //REPLACE
            var a = "Hola a todos <br>";
            document.write(a.replace("todos", "mis amigos")); // Hola a mis amigos

        //SLICE --> Muestra la cadena a partir de una posicion
            var a = "Hola a todos <br>";
            document.write(a.slice(5)); // a todos

        //SPLIT --> COnvierte una cadena a Array
            var a = "Lunes Martes Miercoles Jueves"
            console.log(a.split(" "));

        //CONCAT
            var a = "Youtube";
            document.write(a.concat(", Hola a mis amigos que ven mis videos"));
            document.write("Hola a mis amigos de " + a + " que me siguen")
            document.write(`Hola a mis amigos de ${a} que me siguen en mi canal`);

        //Array
            var uno = [10,20,30,40,50];

            uno.push(60);       //Agregamos un valor
            uno.pop();          //Sacamos el ultimo valor

            //Join --> Convierte un array a cadena
            var uno = [10,20,30,40,50, "Lunes"];
            console.log(uno.join());

            //Array.from
            HTML
                // <div class="dias">
                //     <p>Lunes, Martes, Miercoles, Jueves, Vierner, Sabado, Domingo</p>
                // </div>
            JS
                var semana = Array.from(document.querySelectorAll(".dias p"));
                var creacion = semana.map(dia => dia.textContent);

    //CICLOS
        var uno = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
        
        //FOR
            for(let i in uno){
                document.write(uni[i] + "<br>");
            }
        
        //FOREACH
            uno.foreach(
                (dos,i)=> document.write(i + " - " + dos + "<br>")
            );

        //FIND
            var buscar = uno.find(dos=> dos =="Martes");
            document.write(buscar);     //Devuelve la palabra si la encuentra (Martes)

            var buscar = uno.findIndex(dos=> dos =="Martes");
            document.write(buscar);     //Devuelve el indice si la encuentra (1)

            var buscar = uno.filter(dos=> dos =="Martes");
            document.write(buscar);     //Devuelve la palabra si la encuentra mas de una vez

    //Programacion Orientada a Objeto
        //CLASS
        class Animal{
            constructor(tipo, raza){
                this.tipo = tipo;
                this.raza = raza;
            }
        }

        class Domesticos extends Animal{
            constructor(tipo, nombre, edad, peso){
                super(tipo)
                this.nombre = nombre;
                this.edad = edad;
                this.peso = peso;
            }
        }
        
        var animal1 = new Domesticos("Hogar", "Boby", 8, 6);
        var animal2 = new Domesticos("Caza", "Jorge", 8, 6);

        console.log(animal1);

    //BOM (Browser Object Model)
        function visitar(){
            ventana = windows.open("ayuda.html", "_blanck", "width-500 height-300")
        }

        ventana.open();
        ventana.close();
        ventana.print();    //Imprime lo que esta en pantalla

        var localizacion = window.location;
        document.write(localizacion); //file:///c:/users/miuser/Desktop/JS/index.html

        var navegator = windows.navigator.vendor;
        console.log(navegator);     //Obtenemos los datos del navegador

    //DOM (Document Object Model)
        //QUERYSELECTOR
            //Class
            var a = document.querySelector(".primero");
            var a = document.querySelector("a[class='vinculo']");
            a.style.color = "red";

            //id
            var a = document.querySelector("#segundo");
            a.style.color = "red";

            //Tags
            var a = document.querySelector("h1"); //Afecta a la primera etiqueta
            var a = document.querySelector(".ultima h1");
            a.style.color = "red";
        
        //GETELEMENTBYID
        var a = document.getElementById("dos"); //Aca Seleccionamos un ID

        var a = document.getElementsByClassName("primero")[1]; //Aca Seleccionamos por clase pero debemos especificar cual debe ser

        var a = document.getElementsByTagName("p")[2]; //Aca seleccionamos una etiqueta tipo parrafo

        //agregar contenido
        a.innerHTML += "<p>NUEVO CONTENIDO</p>"

        //cambiar nombre a una clase
        a.className = a.className.replace("primero", "nueva");