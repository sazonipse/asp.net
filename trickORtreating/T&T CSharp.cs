//<CONSOLA>
    Console.WhiteLine("");
    Console.ReadLine("");
//<FIN CONSOLA>


//<CONVERTIR ENTRE TIPOS DE DATOS>
    bool conversion = bool.Parse("false");
//<FIN CONVERSION>


//<CONDICIONALES>
    if(string.IsNullOrEmpty(nombre)){
        saludo += "Desconocido"
    } else {
        saludo += nombre;
    }

    saludo += string.IsNullOrEmpty(nombre) ? "Desconocido" : nombre;

    if(numm == 0){
    }else if (numm >= 5){
    }

    switch (numero)
    {
        case 1: break;
        case 2: break;
        default: break;
    }
//<FIN CONDICIONALES>


//<CICLOS>
    for (i=10;i>=0;i--){ }
    for(int i =0; i<=10, i++){}
    for(int i=0;j=0;i<10 || j>-10;i++,j--){}


    while(i<=10){
        i++;
    }

    do{
        i++;
    } while(i<=10)
//<FIN CICLOS>


//<CADENAS>
    //SPLIT
        string cadena ="1 2 3 4 5 6";
        string[] trozos = cadena.Split('');
        foreach(string strozo in trozos){
            Console.WriteLine(strozo);
        }

        string cadena ="1;2;3;4;5;6;;;";
        string[] trozos = cadena.Split(new char[]{';'}, StringSplitOption.RemoveEmptyEntries);
        foreach(string strozo in trozos){
            Console.WriteLine(strozo);
        }

    //StringBuilder
        StringBuilder stb = new StringBuilder();
        for (int I = 0; i<100; i++){
            stb.Append(i);

            stb.Append("mas texto");

            stb.Remove(0, stb.Length-1);
        }


    //Array de string
        string[] ListaCadenas = new string[]{"hola","adios","juanito","OtroValor"}
        //Si no se sabe la longitud ListaCadena.Length
        for(int i 0 0; i<4; i++){Console.WriteLine(ListaCadenas[i]);}

        //Copiar una cadena
        string[] ListaCadenaGrande = new string[15];
        ListaCadenas.Copyto(ListaCadenaGrande,0);
        for(int i = 0; i<ListaCadenaGrande.Lenght; i++){Console.WriteLine(ListaCadenaGrande[i]);}

    //Array de Entero
        int[] ListaNumeros = new int[1,2,3,4,5]
        foreach(int numero in ListaNumeros){Console.WriteLine("Hola" + numero);}

    //Array Dinamico - Se debe usar como un objeto
        ArrayList miArrayDinamico = new ArrayList();
        miArrayDinamico.Add("Manolo");
        miArrayDinamico.Add(5);
        miArrayDinamico.Add(false);
        miArrayDinamico.Add(new DateTime (10,10,2));

        miArrayDinamico.AddRange(ListaNumeros);//Agrega la lista completa de valores

        //Convertir a un valor
        int nExtrae = (int)miArrayDinamico[1];

        for(int i=0; i<miArrayDinamico.Count;i++){ Console.WriteLine(miArrayDinamico[i]);}
        foreach(object obj in miArrayDinamico){Console.WriteLine(obj)}

        //Remover valor
        miArrayDinamico.Remove(3);  //Elimina el que tenga valor 3
        miArrayDinamico.RemoveAt(3) //Elimina la posicion indice 3

    //List
        List<string> miListStrings = new List<string>(){"Hola", "Adios", "Juanito"};
        miListStrings.Add("Otro");
        foreach(string elemento in miListStrings){Console.WriteLine(elemento); }

        List<int> miListaInt = new List<int>(){1,2,3}:
        miListaInt.Add(4);
        foreach(int numero in miListaInt){Console.WriteLine(numero);}
    
//<FIN CADENAS>


//<FUNCIONES>
    private static int DevuelvemeUnNumero(){
        return 5;
    }
    //Con variables - Copia del valor
    private static int DevuelvemeUnNumero(int numero){
        return numero;
    }

    //Con referencia
    SumarDos(ref numero);
    private static void SumarDos(ref int numero){
        numero +=2
    }

    //Parametro OUT
    SumarDosOUT(out numero)
    private static void SumarDosOUT(out int numero){
        numero =2
    }

    //Array
    FuncionArray(new string[]{"uno", "dos", "tres"});
    private static void FuncionArray(string[] cadenas){
        foreach (string cadena in cadenas){
            Console.WriteLine(cadena);
        }
    }


    FuncionArray("uno", "dos", "tres")
    private static void FuncionArray(params string[] cadenas){
        foreach (string cadena in cadenas){
            Console.WriteLine(cadena);
        }
    }
//<FIN FUNCIONES>


//<CLASES>
    //Para tener acceso deben estar en public
    public class Student{
        public void Matricular(){}
    }

    public partial class Student{
        public void test(){
            this.Name = "Hola";
        }
    }

    //Asignar valor por propiedades
        private string _name;
        
        public string Name{
            get{return _name;}

            set{_name = value;}
        }

        //O simplificado
        public string Name{
            get; private set;
        }

        public student(string name){//Constructor
            this.Name = name;
        }
    
    
    //conversiones explicitas
    private static void workWithPersons(List<Person> personas){
        foreach(Person person in persons){
            //metodo1
                if(person is Teacher){
                    Teacher teacher = (Teacher)person;
                    Console.WriteLine("Profesor " + teacher.Name);
                } else if(person is Student){
                    Student student = (Student)person;
                    Console.WriteLine("Estudiante " + student.Name);
                }
            //metodo 2
                Teacher teacherAs = person as teacher;
                if(teacherAs != null){
                    Console.WriteLine("Profesor " + teacherAs.Name);
                } else {
                    Student student = person as Student;
                    Console.WriteLine("Estudiante " + studentAs.Name);
                }
        }
    }
//<FIN CLASES>


//<>
//<FIN>