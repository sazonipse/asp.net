@Html.TextBox("Nombre", "Ingresa tu Nombre", new { @class="form-control" })
@HTML.TextBoxFor(d=>d.Nombre, new { @Class= "form-control" })
<input class="form-control" id="Nombre", name= "Nombre", type="text" Value="" />


//< USO DE FECTH >//
    //En el modelo Models/ViewModel/ListPeopleViewModel.cs
    public class ListPeopleViewModel{
        public int Id {get; set;}
        public string Name {get; set;}
        public int Age {get; set;}
    }

    //En el controlador
    public ActionResult List(){
        List<ListPeopleViewmodel>lst = new List<ListPeopleViewmodel>();

        using (CrudMVCRazorFetchEntities db = new CrudMVCRazorFetchEntities())
        {
            List<ListPeopleViewmodel> lst = (
                from d in db.People
                orderby d.id descending
                select new ListPeopleViewmodel{
                    Id = d.id,
                    Name = d.name,
                    Age = d.age
                }).ToList();
        }

        return View(lst);
    }

    //En el index
    ....
    <div id="divLista" style="border:1px solid #999;"> </div>
    ....
    <script>
        urlGet = "@Url.Content("~/Peple/List")"
        GET();

        fetch(urlGet(){
            .then(function (response){
                return response.text();
            })
            .then((data) => {
                document.getElementById("divLista").innerHTML = data;
            })
        })
    </script>
//< FIN DE FETCH >//


//<>//
//<>//


//<>//
//<>//


//<>//
//<>//


//<>//
//<>//


//<>//
//<>//

//<>//
//<>//


//<>//
//<>//

