--<BASE DATOS: Crear una Base de Datos>--
CREATE DATABASE 70-461
--<>--



--< TABLAS: Las tablas de variables no son afectadas por el ROLLBACK >--
    --LOCAL
        CREATE TABLE #tablaLocal (ID int, A varchar(10))
    --GLOBAL
        CREATE TABLE ##tablaGlobal(ID int, A varchar(10))
    --Variable
        DECLARE @tablaVariable TABLE(ID int, A varchar(10))
--<FIN TABLAS>--



--< INDEX: Creacion de indices en SQL >--
    --CLUSTER
        SET STATISTICS IO ON
        SET STATISTICS TIME ON

        CHECKPOINT
            DBCC DROPCLEANBUFFERS
            DBCC FREEPROCCACHE
        SELECT ID_CLI, FX_FACT FROM dbo.FACT WHERE ID_CLI = 7

        SET STATISTICS IO OFF
        SET STATISTICS TIME OFF

        /* CUANDO USA
            - OPERADORES >, <, >=, <= Y BETWEEN
            - MUCHOS DATOS O COLUMNAS
            - JOIN
            - ORDER BY
        */
    --NO CLUSTER
        CREATE INDEX IDX_CLI ON FACT(ID_CLI)
        DROP INDEX IDX_CLI ON dbo.FACT

        /*CUANDO USAR
            - JOIN
            - GROUP BY
            - SELECT *
        */

    --CLUSTER VS NO CLUSTER
        CREATE INDEX IDX_CLI ON FACT(ID_CLI)

        CHECKPOINT
            DBCC DROPCLEANBUFFERS
            DBCC FREEPROCCACHE
        
        SELECT ID_CLI, FX_FACT FROM dbo.FACT WHERE ID_CLI = 7

        DROP INDEX IDX_CLI ON dbo.FACT
    
    --MULTIPLE
        CREATE INDEX IDX_CLI_IMP ON dbo.FACT(ID_CLI, IMPORTE)
            SELECT ID_CLI, IMPORTE, FX_FACT FROM dbo.FACT WHERE ID_CLI = 7 AND IMPORTE BETWEEN 1000000 AND 2000000
        DROP INDEX IDX_CLI_IMP ON dbo.FACT

    --INCLUDE
        CREATE INDEX IDX_ID_INCLUIDO ON FACT (ID) INCLUDE (ID_CLI, IMPORTE)
        SELECT ID_CLI, IMPORTE FROM dbo.FACT WHERE ID BETWEEN 1 AND 1000

    --FILTRADOS
        SET STATISTICS IO ON
        SET STATISTICS TIME ON
        CHECKPOINT
            DBCC DROPCLEANBUFFERS
            DBCC FREEPROCCACHE
        
        CREATE INDEX IDX_CLI_FILTRADO ON dbo.FACT (ID_CLI) INCLUDE (IMPORTE) WHERE ID_CLI = 1

        SELECT ID_CLI, IMPORTE, FX_FACT FROM dbo.FAC WHERE ID_CLI = 1

        DROP INDEX IDX_CLI_FILTRADO ON dbo.FACT
        SET STATISTICS IO OFF
        SET STATISTICS TIME OFF
--< FIN INDEX >--



--< VISTAS CON INDEX >--
    /*  DATOS
        - NO ACEPTA PARAMETROS
        - NO EXISTE ORDER BY EXCEPTO SI SE USA TOP
        - SE PUEDE CREAR INDICES: EN AGREGACIONES(SUM, AVG,...)
    */
    --CLUSTER
        IF EXISTS(SELECT * FROM sys.views WHERE name = 'V_INDEXADA')
            DROP VIEW V_INDEXADA
        GO
        /*SCHEMABINDING: Se utiliza para evitar que se modifiquen las tablas mientras se ejecuta el procedimiento*/
        CREATE VIEW V_INDEXADA WITH SCHEMABINDING
        AS
            SELECT ID_CLI,FX_FACT, SUM(IMPORTE) TOTAL_POR_CLIENTE_Y_DIA, COUNT_BIG(*) cnt FROM dbo.T1 GROUP BY ID_CLI, FX_FACT
        GO
        
        CREATE UNIQUE CLUSTERED INDEX IDX_V ON dbo.V_INDEXADA (ID_CLI, FX_FACT)

        SET STATISTICS TIME ON 
        SET STATISTICS IO ON

        CHECKPOINT
            DBCC DROPCLEANBUFFERS
            DBCC FREEPROCCACHE
        SELECT * FROM dbo.V_INDEXADA

        SET STATISTICS TIME OFF 
        SET STATISTICS IO OFF
--< FIN VISTAS >--


--< PROCEDIMIENTOS >--
    IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'B')
        DROP PROCEDURE B
    GO
    
    --LOCAL
        CREATE PROC #temporalLocal 
        AS 
            SELECT * FROM sys.objects
        GO
    
    --GLOBAL
        CREATE PROC ##temporalGlobal
        AS
            SELECT * FROM sys.objects
        GO
        DROP PROC ##temporalGlobal

    --INSERT
        IF EXISTS (SELECT * FROM sys.tables WHERE name = 'NOMBRES')
            DROP TABLE dbo.NOMBRES
        GO

        IF EXISTS(SELECT * FROM dbo.procedures WHERE name = 'A')
            DROP PROCEDURE A
        GO

        CREATE PROCEDURE A (@OBJ_ID INT)
        AS
            SELECT NAME FROM sys.objects WHERE object_id = @OBJ_ID
        GO

        INSERT INTO NOMBRES (NAMES)
            EXEC A @OBJ_ID = 1781581385
        GO

        SELECT * FROM dbo.NOMBRES
--< FIN PROCEDIMIENTOS >--



--< FUNCIONES >--
    --ESCALAR: Son las mas antiguas
        /*
            Tabla
            A       B
            16      7
            16      9
            16      47
            8       93
        */
        CREATE FUNCTION FUNCION_ESCALAR(@X CHAR(10)) RETURNS VARCHAR(100)
        AS BEGIN
            DECLARE @A VARCHAR(100)
            SET @A=''
            SELECT @A=@A+RTRIM(B)+',' FROM DATOS WHERE A=@X
            RETURN @A
        END
        /*
            Esto devolveria
            16      7,9,47,
            8          93,
        */
    
    --TABLA EN LINEA: Son las mas eficientes
        CREATE FUNCTION FUNCION_ENLINEA(@X CHAR(10)) RETURNS TABLE
        AS
            RETURN
                SELECT K FROM(SELECT RTRIM(B)+','
                              FROM DATOS
                              WHERE A=@X FOR XML PATH('')) AS X(K)
        GO

    --TABLA MULTILINEA: Hay que saber programarla, se puede agregar mas logica, condiciones o bucles
    CREATE FUNCTION FUNCION_MULTILINEA (@X CHAR(10))
    RETURNS @T TABLE( K VARCHAR(5000))
    AS BEGIN
        INSERT INTO @T(K)
            SELECT K FROM (SELECT RTRIM(B) +',' [text()] FROM DATOS WHERE A=@X FOR XML PATH('')) AS X(K)
        RETURN
    END
--< FIN FUNCIONES >--



--< OUTPUT >--
    --INSERT
        IF EXISTS (SELECT * FROM sys.tables WHERE name = 'T_OUTPUT')
            DROP TABLE dbo.T_OUTPUT

        CREATE TABLE T_OUTPUT(
            id INT IDENTITY (1,1) PRIMARY KEY,
            A VARCHAR(100)
        );
        GO

        INSERT INTO T_OUTPUT (A)
            OUTPUT Insert.id, Inserted.A
            SELECT TOP 10 name FROM sys.tables
    
    --INTO
        DECLARE @T_OUTPUT AS TABLE(
            id INT,
            A VARCHAR (100)
        )
        INSERT INTO T_OUTPUT (A)
            OUTPUT Insert.id, Insert.A
            INTO @T_OUTPUT
            SELECT TOP 10 name FROM sys.tables

            SELECT 'select sobre la varaible de tabla', * FROM @T_OUTPUT

    --UPDATE
        UPDATE dbo.T_OUTPUT SET A = 'HOLA' 
        OUTPUT Insert.id, Insert.A valor_de_A_nuevo, Deleted.A valor_de_A_antiguo
        WHERE ID = 1

    --DELETE
        DELETE FROM T_OUTPUT
        OUTPUT Deleted.*
        WHERE id > 1
--< FIN OUTPUT >--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--



--<>--
--<>--
